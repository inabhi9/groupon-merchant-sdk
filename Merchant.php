<?php
namespace Groupon;

use \GuzzleHttp\Client;


class Merchant {
    private $gClient;
    private $token;
    private $username;
    private $password;
    private $merchantId;

    function __construct($username, $password) {
        $this->gClient = new Client([
            'base_url' => ['https://merchants.groupon.com/{version}/', ['version' => 'v2']],
            'defaults' => ['cookies' => true]
        ]);

        $this->username = $username;
        $this->password = $password;
    }

    /**
     * Authorize user
     */
    public function authorize() {
        $response = $this->gClient->post('oauth/authorize?ajax=true', [
            'body' => [
                'username'      => $this->username,
                'password'      => $this->password,
                'response_type' => 'token',
                'redirect_uri'  => 'https://merchants.groupon.com/login'
            ]
        ]);
        $this->token = $response->json()['auth']['accessToken'];
        $this->gClient->setDefaultOption('headers', ['Authorization' => 'OAuth ' . $this->token]);
    }

    /**
     * Return logged in user information
     *
     * @return mixed
     */
    public function getMe() {
        $response = $this->gClient->get('users/me');

        return $response->json();
    }

    /**
     * Set merchant id. It's being used to in many functions.
     *
     * @param $merchantId
     */
    public function setMerchantId($merchantId) {
        $this->merchantId = $merchantId;
    }

    /**
     * List merchants deals
     *
     * @param $options Filter options
     *      filter_now_deals: boolean
     *      filter_reward_deals: boolean
     *      filter_stores_deals: boolean
     *      filter_deal_status: Comma separated status.
     *          Valid statues: draft,deleted,submitted,approved,rejected,inprocess
     *      page: Page to navigate
     *      per_page: Results per page
     * @return mixed
     */
    public function listDeals($options) {
        $response = $this->gClient->get("merchants/{$this->merchantId}/deals", [
            'query' => $options
        ]);

        return $response->json();
    }

    /**
     * Download deal voucher list in CSV format
     *
     * @param $dealId Deal id to be download
     * @param null $saveTo If it's null, file url will be returned. Otherwise Fill will be saved
     * to this location
     * @return mixed
     */
    public function downloadDealVoucherList($dealId, $saveTo = null) {
        $uri = "merchants/{$this->merchantId}/deals/{$dealId}/customer_list_request";
        $response = $this->gClient->post($uri);
        $url = $response->json()['response']['downloadUrl'];
        if ($saveTo == null)
            return $url;

        // Saving file
        $this->gClient->get($url, [
            'save_to' => $saveTo,
        ]);
    }
}

?>
